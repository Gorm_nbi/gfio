package gfio

import "os"
import "fmt"
import "strings"
import "time"

// Write flags and provided data to file with name fn.
func SaveDataJSON(fn, names string, data ...interface{}) {
	// Create file and encoder
	f, err := os.Create(fn)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error creating data-file:", err)
		os.Exit(1)
	}

	// Define function that check and prints writing errors
	checkErr := func(err error) {
		if err != nil {
			fmt.Fprintln(os.Stderr, "Error writing to file:", err)
			os.Exit(1)
		}
	}

	// Define function that adds json field
	addField := func(name string, data interface{}) {
		_, err := fmt.Fprintf(f, "%q:", name)
		checkErr(err)
		Encode2json(f,data)
		fmt.Fprint(f,",\n")
	}	

	// Start json object
	_, err = fmt.Fprint(f, "{")
	checkErr(err)

	// Write flags to file (in order of definition)
	{
		_, err := fmt.Fprintf(f, "%q:{", "metadata", )
		checkErr(err)
		for i,flag := range flagnames {
			if i>0 {
				_, err := fmt.Fprint(f,",")
				checkErr(err)
			}
			_, err := fmt.Fprintf(f, "%q:%s", flag,flags[flag].val)
			checkErr(err)
		}
		fmt.Fprint(f,"},\n")
	}

	// Split data in named and unnamed lists
	unnamed := make([]interface{}, 0, len(data))
	splitNames := strings.Split(names, ",")
	if len(splitNames) > len(data) {
		addField("unused_data_names",splitNames[len(data):])
		splitNames = splitNames[0:len(data)]
	}
	
	// Write named data to file
	for i, name := range splitNames {
		name := strings.TrimSpace(name)
		if name != "" {
			addField(name, data[i])
		} else {
			unnamed = append(unnamed, data[i])
		}
	}

	// Write unnamed data to file
	unnamed = append(unnamed, data[len(splitNames):]...)
	if len(unnamed) > 0 { 
		addField("data", unnamed)
	}

	// Write data from temporary datafiles
	for _,tmp := range tmpDataFiles {
		tmp .CopyNClose(f)
	}
	
	// Write time-stamp
	fmt.Fprintf(f,"%q:%q\n", "timestamp", time.Now().String())

	// End json object and close file
	_, err = fmt.Fprintln(f, "}")
	checkErr(err)
	err = f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error closing data-file:", err)
		os.Exit(1)
	}
}
