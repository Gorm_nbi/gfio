package gfio

import (
	"fmt"
	"strings"
	"os"
)

// EnableHelp enables the "help" flag.
// It also prints options and exits program if flag is provided but not used.
func EnableHelp() {
	helpflag := flags["help"]
	helpflag.used = true
	flags["help"] = helpflag

	var print_flaglist bool
	// detect and handle "flag provided but not used"
	provided_not_used := make([]string,0)
	for flag, flagInfo := range flags {
		if flagInfo.provided && !flagInfo.used {
			provided_not_used = append(provided_not_used, flag)
		}
	}
	if len(provided_not_used) > 0 {
		print_flaglist = true
		fmt.Println("\nFlag(s) provided but not used:")
		for _,flag := range provided_not_used {
			fmt.Println(" ", flag)
		}
		fmt.Print("\n")
	}
	// handle detailed help request
	if flags["help"].provided {
		print_flaglist = true
		harg := flags["help"].val
		if flags[harg].used {
			flag := flags[harg]
			// print full flag description
			fmt.Printf("flag name     : %s\n", harg)
			fmt.Printf("variable type : <%s>\n", flag.typ)
			fmt.Printf("default value : %v\n\n", flag.def)
			fmt.Println("Description:")
			for _,d := range flag.description {
				fmt.Println(d)
			}
			os.Exit(0)
		}else if flags[harg].val != "" {
			fmt.Println("unknown flag:", harg)
		}
	}
	// pretty-print flag options
	if print_flaglist {
		if len(flagnames) == 0 {
			fmt.Println("\nno flags defined\n")
			os.Exit(0)
		}
		fmt.Println("\nOptions:")
		var cap_name int = 4
		var cap_type int = 4
		var cap_dflt int = 7
		var cap_dscr int
		for _, flag := range flagnames {
			flagInfo := flags[flag]
			if len(flag) > cap_name {
				cap_name = len(flag)
			}
			if len(flagInfo.typ) > cap_type {
				cap_type = len(flagInfo.typ)
			}
			if len(flagInfo.def) > cap_dflt {
				cap_dflt = len(flagInfo.def)
			}
			// 80 is a magic standard and 10 is for spacing
			cap_dscr = 80 - cap_name - cap_type - cap_dflt - 10
			if cap_dscr < 4 {
				cap_dscr = 4
			}
		}
		// column headlines
		fmt.Printf("%s%s  %s%s  %s  %s | %s\n\n",
			"Flag", strings.Repeat(" ", cap_name-4),
			strings.Repeat(" ", cap_dflt-7), "Default",
			"Type", strings.Repeat(" ", cap_type-4),
			"Description",
		)
		// print flags-options
		var more_description bool
		for _,flag := range flagnames {
			flagInfo := flags[flag]
			var description string = ""
			if len(flagInfo.description) > 0 {
				description = flagInfo.description[0]
				if len(flagInfo.description) > 1 {
					description += "*"
					more_description = true
				}
			}
			if len(description) > cap_dscr {
				description = description[:cap_dscr-4]+"...*"
				more_description = true
			}
			fmt.Printf("%s%s: %v%s  <%s>%s | %s\n",
				flag, strings.Repeat(".", cap_name-len(flag)),
				strings.Repeat(" ", cap_dflt-len(flagInfo.def)), flagInfo.def,
				flagInfo.typ, strings.Repeat(" ", cap_type-len(flagInfo.typ)),
				description,
			)
		}
		if more_description {
			fmt.Println("\n*: more description available. Try help=<flag>")
		}
		os.Exit(0)
	}
}
