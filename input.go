// The idea with this package is to combine the input featurs of "flag" with the construction of a data-structure which can easily be converted to json and saved
package gfio

import (
	"fmt"
	"os"
	"reflect"
	"strconv"
)

func Str(name string, def string, description ...string) string {
	parseAny(name, &def, description)
	return def
}

func Int(flag string, def int, description ...string) int {
	parseAny(flag, &def, description)
	return def
}

func F64(flag string, def float64, description ...string) float64 {
	parseAny(flag, &def, description)
	return def
}

func parseAny(name string, p interface{}, description []string) {
	// Get value of pointer argument a reflect.Value
	rp := reflect.ValueOf(p)
	// Make sure pointer argument is actually a pointer
	if rp.Kind() != reflect.Ptr {
		panic("not a pointer")
	}
	// Retrieve information about flag
	flag := flags[name]
	// Make sure flag is not used multiple times
	if flag.used {
		fmt.Fprintln(os.Stderr,
			"Multiple flag-variables defined with the same name: "+name)
		os.Exit(1)
	}
	// Write default-value
	flag.typ = fmt.Sprint(rp.Elem().Type())
	flag.def = fmt.Sprint(rp.Elem())
	flag.description = description
	// Parse flag value string or store default as string in flag map
	if !flag.provided {
		flag.val = flag.def
	} else {
		var err error
		switch tp := p.(type) {
		case *string:
			*tp = flag.val
		case *int:
			*tp, err = strconv.Atoi(flag.val)
		case *float64:
			*tp, err = strconv.ParseFloat(flag.val, 64)
		default:
			panic("unexpected type")
		}
		if err != nil {
			fmt.Fprintln(os.Stderr, "Invalid", rp.Type().Elem(), "flag value:", flag.val, err)
			os.Exit(1)
		}
	}
	// Mark flag as used and write back changes to flag map
	flag.used = true
	flags[name] = flag
	flagnames = append(flagnames,name)
}
