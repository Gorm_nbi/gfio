package gfio

import "os"
import "fmt"
import "io"
import "sync"

type tmpdatafile interface {
	CopyNClose(io.Writer) 
}

// Such a struct should never be copied!
// The user should ONLY have access to pointers.
type dataList struct {
	mtxR,mtxW    sync.Mutex
	f            *os.File
	entry_count  int
}

// Init a new temporary data-file with the purpose of being the entries in a json list
func NewDataList(name string) (DL *dataList) {
	f,err := os.Create("temp_"+name)
	if err != nil {
		panic(err)
	}
	DL = new(dataList)
	DL.f = f
	tmpDataFiles = append(tmpDataFiles,DL)
	return
}

func (DL *dataList) Append(data interface{}) {
	DL.mtxW.Lock()
	DL.mtxR.Lock()
	if DL.entry_count > 0 {
		fmt.Fprint(DL.f,",")
	}
	Encode2json(DL.f,data)
	DL.entry_count++
	DL.mtxR.Unlock()
	DL.mtxW.Unlock()
}

// Copy the temporary data file to a new destination
// AND delete the temporary data-file
func (DL *dataList) CopyNClose(destination io.Writer) {
	DL.mtxR.Lock()
	var err error
	_,err = DL.f.Seek(0,0)
	_,err = fmt.Fprintf(destination,"%q:[",DL.f.Name()[5:]) // "temp_" is cut off
	_, err = io.Copy(destination, DL.f)
	_,err = fmt.Fprint(destination,"],\n")
	err = os.Remove(DL.f.Name())
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error occured when copying temporary datafile to its destination, and then deleting it.\nError message:\n", err)
		panic(err)
	} 
}


// Such a struct should never be copied!
// The user should ONLY have access to pointers.
type dataDict struct {
	mtxR,mtxW    sync.Mutex
	f            *os.File
	entry_count  int
}

// Init a new temporary data-file with the purpose of being the entries in a json dictionary
func NewDataDict(name string) (DD *dataDict) {
	f,err := os.Create("temp_"+name)
	if err != nil {
		panic(err)
	}
	DD = new(dataDict)
	DD.f = f
	tmpDataFiles = append(tmpDataFiles,DD)
	return
}

func (DD *dataDict) Append(name string, data interface{}) {
	DD.mtxW.Lock()
	DD.mtxR.Lock()
	if DD.entry_count > 0 {
		fmt.Fprint(DD.f,",")
	}
	fmt.Fprintf(DD.f,"%q:", name)
	Encode2json(DD.f,data)
	DD.entry_count++
	DD.mtxR.Unlock()
	DD.mtxW.Unlock()
}

// Copy the temporary data file to a new destination
// AND delete the temporary data-file
func (DD *dataDict) CopyNClose(destination io.Writer) {
	DD.mtxR.Lock()
	var err error
	_,err = DD.f.Seek(0,0)
	_,err = fmt.Fprintf(destination,"%q:{",DD.f.Name()[5:]) // "temp_" is cut off
	_, err = io.Copy(destination, DD.f)
	_,err = fmt.Fprint(destination,"},\n")
	err = os.Remove(DD.f.Name())
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error occured when copying temporary datafile to its destination, and then deleting it.\nError message:\n", err)
		panic(err)
	} 
}
